package com.bugsnag.demo.jetty.embeddedjetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

import com.bugsnag.demo.jetty.embeddedjetty.servlet.ExampleServlet;


public class EmbeddedJettyMain {

	public static void main(String[] args) throws Exception {
		Server server = new Server(7010);
		ServletContextHandler handler = new ServletContextHandler(server, "/slack");
		handler.addServlet(ExampleServlet.class, "/");
		server.start();
	}

}
